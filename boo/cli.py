"""Command line entry point"""

import click
import yaml

import boo.markov
import boo.twitter


def send_status(twitter, status, hashtag=None):
    result = twitter.status(status, hashtag=hashtag)
    print("Posted '{}' to twitter: http://twitter.com/{}/status/{}".format(
        result['text'], result['user']['screen_name'], result['id_str']))
    return result


@click.group()
@click.pass_context
def main(ctx):
    """Collects tweets and uses a markov chain to generate new tweets"""
    ctx.obj = boo.twitter.Twitter.from_auth_file()


@main.command()
@click.pass_obj
def collect(twitter):
    """Collects more tweets from the timeline"""
    twitter.load_tweets()
    twitter.collect_tweets()
    twitter.save_tweets()


@main.command()
@click.pass_obj
def generate(twitter):
    """Generates a tweet from the collected tweets"""
    tweets = twitter.load_tweet_texts()
    print(boo.markov.generate_message(tweets))


@main.command()
@click.option(
    '--hashtag', '-t', default='#boo',
    help="A hashtag to add to the status")
@click.pass_obj
def send(twitter, hashtag):
    """Generates and posts a tweet"""
    tweets = twitter.load_tweet_texts()
    message = boo.markov.generate_message(tweets)
    send_status(twitter, status, hashtag)


@main.command()
@click.option(
    '--hashtag', '-t', default='#boo',
    help="A hashtag to add to the status")
@click.argument('status', nargs=-1)
@click.pass_obj
def status(twitter, hashtag, status):
    """Posts an arbitary status"""
    send_status(twitter, ' '.join(status), hashtag)


if __name__ == '__main__':
    main()

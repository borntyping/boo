import os.path

import twython
import yaml


class Twitter:
    @classmethod
    def from_auth_file(cls, filename='auth.yaml'):
        with open(filename, 'r') as f:
            return cls(**yaml.load(f))

    def __init__(self, app_key, app_secret, access_token, access_secret):
        self.api = twython.Twython(
            app_key, app_secret, access_token, access_secret)
        self.tweets = []

    def load_tweets(self, filename='data.yaml'):
        if not os.path.exists(filename):
            with open(filename, 'w') as f:
                yaml.dump(self.fetch_tweets(), f)

        with open(filename, 'r') as f:
            self.tweets = list(yaml.load(f))

    def load_tweet_texts(self, filename='data-text.yaml'):
        with open(filename, 'r') as f:
            return list(yaml.load(f))

    def save_tweets(self, filename='data.yaml', text_filename='data-text.yaml'):
        with open(filename, 'w') as f:
            yaml.dump(self.tweets, f)

        with open(text_filename, 'w') as f:
            yaml.dump([tweet['text'] for tweet in self.tweets], f)

    def fetch_tweets(self, **options):
        return self.api.get_home_timeline(
            count=200, trim_user=True, include_entities=False, **options)

    def collect_tweets(self):
        new_tweets = self.fetch_tweets(
            since_id=max(t['id'] for t in self.tweets))
        old_tweets = self.fetch_tweets(
            max_id=min(t['id'] for t in self.tweets))
        self.tweets = new_tweets + self.tweets + old_tweets

    def status(self, status, hashtag=None):
        if hashtag and len(status) < (139 - len(hashtag)):
            status = "{} {}".format(status, hashtag)
        return self.api.update_status(status=status)

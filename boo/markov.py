import markovgen

def generate_message(messages):
    return markovgen.Markov(messages).generate_markov_text()

#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name='boo',
    version='0.0.0-dev',

    author="Sam Clements",
    author_email="sam.clements@datasift.com",

    url="https://github.com/borntyping/boo",
    description="boo!",
    long_description=open('README.rst').read(),
    license="MIT",

    packages=setuptools.find_packages(),

    install_requires=[
        'click',
        'markovgen',
        'pyyaml',
        'twython'
    ],

    entry_points={
        'console_scripts': [
            'boo = boo.cli:main',
        ]
    },

    classifiers=[
        'Development Status :: 1 - Planning',
        'License :: OSI Approved',
        'License :: OSI Approved :: MIT License',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Software Development :: Libraries'
    ]
)
